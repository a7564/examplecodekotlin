package com.example.accessibilityservice01

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.AccessibilityServiceInfo
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.util.Log
import android.view.accessibility.AccessibilityEvent

class MyAccessibilityService : AccessibilityService() {
    private  val tagName = "MyAccessibilityService:"
    override fun onAccessibilityEvent(p0: AccessibilityEvent?) {
        Log.d(tagName, "onAccessibilityEvent")
        val packageName : String = p0?.packageName.toString()
        val packageManager : PackageManager = this.packageManager
        try {
            val applicationInfo : ApplicationInfo = packageManager.getApplicationInfo(packageName,0)
            val applicationLabel : CharSequence = packageManager.getApplicationLabel(applicationInfo)
            Log.d(tagName, "app name: $applicationLabel")
        }catch (e : PackageManager.NameNotFoundException){
            Log.d(tagName, "NameNotFoundException: $e")
            e.printStackTrace()
        }
    }

    override fun onInterrupt() {
        Log.d(tagName, "onInterrupt")
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d(tagName, "onUnbind")
        return super.onUnbind(intent)
    }
    override fun onServiceConnected() {
        val info  = AccessibilityServiceInfo()
        info.apply {
            // Set the type of events that this service wants to listen to. Others
            // won't be passed to this service.
            eventTypes = AccessibilityEvent.TYPE_VIEW_CLICKED or AccessibilityEvent.TYPE_VIEW_FOCUSED
            // Set the type of feedback your service will provide.
            feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC
            notificationTimeout = 100
        }
        this.serviceInfo = info
        Log.d(tagName, "onServiceConnected")
    }
}