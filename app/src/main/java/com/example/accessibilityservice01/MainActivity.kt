package com.example.accessibilityservice01

import android.accessibilityservice.AccessibilityServiceInfo
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.accessibility.AccessibilityManager
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private  val tagName = "MainActivity:"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn = this.findViewById(R.id.allowPermission) as Button
        btn.setOnClickListener {
            startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS))
        }

        if (!checkAccessibilityPermission()) {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            btn.isEnabled = true
        } else {
            btn.isEnabled = false
        }
        ///val check : Boolean = runningServices()
      //  Log.e(tagName, "check :$check")
       // hasRemoteAccessibilityPermission(this)
    }

    private fun checkAccessibilityPermission(): Boolean {
        var accessEnabled = 0
        try {
            accessEnabled =
                Settings.Secure.getInt(this.contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED)
        } catch (e: Settings.SettingNotFoundException) {
            e.printStackTrace()
        }
        return if (accessEnabled == 0) {
            // if not construct intent to request permission
//            val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            // request permission via start activity for result
//            startActivity(intent)
            false
        } else {
            true
        }
    }

    private fun runningServices(): Boolean {
        val am = getSystemService(ACCESSIBILITY_SERVICE) as AccessibilityManager
        val runningServices = am.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_GENERIC)
        for (service in runningServices) {
            Log.e(tagName, "loop"+service.id)
            if (runningServices.isEmpty() && service.id != ""){
                // The service is enabled.
                Log.e(tagName, "if true"+service.id)
                return true
            }
        }
        Log.e(tagName, "running : $runningServices")
        return false

    }
    fun hasRemoteAccessibilityPermission(context: Context): Boolean {
        val accessibilityManager = context.getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
        val services = accessibilityManager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_GENERIC)
        Log.e(tagName, "services : $services")
        for (service in services) {
//            if (service.flags and AccessibilityServiceInfo.FLAG_REQUEST_REMOTE_ACCESS != 0) {
//                return true
//            }
        }
        return false
    }


}